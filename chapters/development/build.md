
### Building Code

To get software running, build tools `SHOULD` be used.
This allows for building and/or testing a project with one command and facilitates dependency management.

#### Build Tools We Are Using at the Moment

- **bash scripting**: (bdnPrint, FontanePrint)
- **eXist**: Ant (SADE)
- **Java**:
  - Maven (TextGrid)
  - gradle (TextGrid)
- **JavaScript**:
  - bower (DARIAH-DE GeoBrowser, tgForms)
  - cake (tgForms)
  - NPM (DARIAH-DE Publikator, tgForms)
  - rake (DARIAH-DE GeoBrowser)
- **Python**:
  - make (Sphinx documentation)
  - PIP (DiscussData)
- **Ruby**: bundler (DARIAH status page)

#### Packaging and Dependency Management

In general, code packages can be installed as site packages (system-wide) or into the app directory
(known as "vendoring" or "bundling") through a packaging system.

Your app `SHOULD` `NOT` depend on system-wide packages. You `SHOULD` declare all dependencies
of your app in a
dependency declaration manifest and ensure that
there are no leaks from the surrounding system ("package isolation").

For example, in Python, package isolation can be ensured by using a
virtual environment (`virtualenv`/`venv`).
A `requirements.txt` configuration file can be used to list required packages
(and their supported version or range of versions)
and can be installed using `pip` `install` `-r` `requirements.txt`.
This allows users to quickly set up everything they need to run the app.

Dependencies are resolved in a transitive manner,
i.e. if package X requires package Y,
and Y requires Z, then Y and Z will be installed
to ensure that X is working.

Some dependency declaration files allow you to differentiate between packages that are essential
for the app to run,
and packages that help with developing/debugging.
It is `RECOMMENDED` to make use of this if applicable.

For example, in the `package.json` configuration
file for the JavaScript package manager `NPM`,
you can differentiate between `dependencies` and `devDependencies`.
Moreover, `peerDependencies` refer to plugins that the application is compatible with
and that can be installed along with the required packages.

##### Keeping Track of Dependencies and Security Vulnerabilities

In order to keep an overview of the software components used in your project, you `SHOULD` provide a
Software Bill of Materials (SBOM) and check for security vulnerabilities regularly.
For more information on this topic, see [Software Bill of Materials (SBOM)](#software-bill-of-materials-sbom).

#### CESSDA's Software Maturity Levels (CA5)

`MUST` be SML5, which is defined as follows:

> Demonstrable usability: A Continuous Integration server job (or equivalent) is available to deploy the
packaged/containerised software. Administrators are notified if deployment fails. Versions of deployed software can be
upgraded/rolled back from a Continuous Integration server job (or equivalent). Data and/or index files can be restored
from a Continuous Integration server job (or equivalent).

##### Actions to Be Taken in RDD

- examples for versions of deployed software: versioning of deb packages
- examples for rollback: rebuild index ElasticSearch from source data, restore database backup
