
### Documentation

#### Policy

The software developed at SUB `SHOULD` be properly documented on different levels.

We provide

- developer documentation which is needed for further development of the software
- admin documentation which describes how to install the software, how to run and/or restart it, and how to test the installation
- server documentation which gives information about the server a software is installed on (if applicable)
- user documentation which helps to use the software

We aim for the following CESSDA Maturity Levels for our documentation:

##### API Documentation

`MUST` be SML2 (CA1.3), which is defined as follows:

> There is external documentation that describes public API functionality and is sufficient to be used by an
> experienced developer. If available, source code is consistently and clearly commented. Source code naming conventions
> are adhered to and consistent.

##### Admin Documentation

`MUST` be SML3 (CA1.2), which is defined as follows:

> There is a deployment and configuration manual that can guide an experienced operational user through deployment,
> management and configuration of the software. Exception and failure messages are explained, but descriptions of
> solutions are not available. Documentation is consistent with current version of the software.

##### User Documentation

`MUST` be SML2 (CA1.1), which is defined as follows:

> There is external documentation that is accessible and sufficient for an expert user to configure and use the
> software for the user’s individual needs. Terminology and methodology is not explained.

`SHOULD` be SML3 (CA1.1), which is defined as follows:

> There is a user manual that can guide a reasonably skilled user through use and customisation of the software to the
> user’s individual requirements. Documentation is consistent with current version of the software.

#### Guidelines

##### General Notions about Documentation

The documentation language is (American) English.
Follow the acknowledged [Documentation Best Practices](https://chromium.googlesource.com/chromium/src/+/HEAD/docs/documentation_best_practices.md).

You `SHOULD` document a language's specifics if you are using a feature that is

- deprecated,
- legacy,
- undocumented, or
- a breaking change compared to a previous version that is still being actively maintained.

Every code repository `MUST` have a README.md file.
If you do not know how to write one, start using [Make a README](https://www.makeareadme.com/).
Then add (if applicable):

- a link to the original repository,
- a link to a demo instance,
- links to the relevant issue tracker/project management systems, and
- badges to CI status.

###### Docs-as-code

Documentation `SHOULD` be placed within the software repository.
Furthermore for the documentation writing, building, testing and publication process, all quality criteria, workflows
and pipelines `SHOULD` be set up alike those for the software itself.
In this way documentation is treated the same way as code including the usage of version control, issue tracker,
code reviews and automated tests.

A presentation of the usage of this approach for a long-term scientific software project is available at [zenodo](https://zenodo.org/records/7260347).
In general, this approach can be applied to small-scale projects as well.

##### Doc Sprints

To ensure thorough documentation of our code doc sprints or other meetings specifically targeted at writing docs can be organized.

##### Developer Documentation

###### Software Architecture

Each software project `SHOULD` provide an architecture diagram that represents its major components and their interaction.
In some cases using tools to generate diagrams such as UML class diagrams may not be possible.

*Examples:*

- [Generating call graphs in eXist-db](https://gitlab.gwdg.de/SADE/SADE/tree/develop/modules/callgraph)

Call diagrams can be useful to follow code and service calls and should exist for every API method.

###### API Documentation

- Provide a fully documented public API, e.g. by using OpenAPI
- The docs `SHOULD` comprise `used` `parameters`, `author` and `@since` annotations
- Links to callers `MUST` `NOT` be listed in the documentation, because this info might become deprecated at any time
- REST-APIs `SHOULD` be documented by using [OpenAPI](https://github.com/OAI/OpenAPI-Specification).

The OpenAPI documentation `SHOULD` be located at `/doc/api` on servers.
We currently use OpenAPI for RESTXQ and JAX-RS service endpoints.

####### Examples

- [Example for Java usind annotations](https://lab.sub.uni-goettingen.de/self-updating-docs.html)

####### Recommended Tooling

- Java: use call stacks of tools like Eclipse
- XQuery / SADE: [Call Graph Module](https://gitlab.gwdg.de/SADE/SADE/-/tree/main/modules/callgraph)

####### OpenAPI for RESTXQ

For documenting RESTXQ APIs in our eXist-db systems, we use [OpenAPI4RESTXQ](https://gitlab.gwdg.de/subugoe/openapi4restxq).
It has to be installed in eXist-db and configured according to the [instructions](https://gitlab.gwdg.de/subugoe/openapi4restxq#use).

####### OpenAPI for CXF Java JAXRS Applications

We have not yet finished the OpenAPI documentation of our Java services, but a few tests with the
[OpenAPI Feature of Apache CXF](https://cxf.apache.org/docs/openapifeature.html) have already been implemented.

The outcome of this OpenAPI documentation `SHOULD` be the automated generation of API documentation,
to be written directly *in the code* as Java Annotation, using the `@Operation` annotation,
see [example](https://github.com/apache/cxf/blob/master/distribution/src/main/release/samples/jax_rs/description_openapi_v3_spring/src/main/java/demo/jaxrs/openapi/server/Sample.java#L65),
and then read and directly used from within a HTML page.
An automatic creation of an [OpenAPI JSON file](https://dev.textgridlab.org/1.0/tgsearch-public/info/openapi.json) `SHOULD`
be possible to use with a [Swagger UI HTML page frontend](https://dev.textgridlab.org/1.0/tgsearch-public/info/api-docs/?url=/1.0/tgsearch-public/info/openapi.json).
We can deliver this page with every service or we can deploy a central Swagger UI service for all the documentation.
For every service endpoint an OpenAPI doc can be provided, such as already done for [TG-search](https://dev.textgridlab.org/1.0/tgsearch-public).

We can also combine this OpenAPI doc with our existing Sphinx API documentation
(such as the [TG-search Sphinx API documentation](https://textgridlab.org/doc/services/submodules/tg-search/docs/index.html)
as done [here](https://sphinxcontrib-openapi.readthedocs.io)).

##### Admin Documentation

- The docs should comprise how to install the software, how to run and/or restart it, how to test the installation, ...
- Short deployment descriptions can be provided in the README
- More detailed deployment explanations should be kept separately (such as INSTALL(.md), linked from README)
- Exception and failure messages are described in doc strings/function annotations
- Function documentation should be generated automatically
- Function documentation should be made available/searchable in the web (such as readthedocs, javadoc html, etc. pp.)

##### User Documentation

This may encompass:

- how to use the software and APIs, FAQs, walkthroughs, ...
- guided tour (e.g. Bootstrap Tour) as user documentation
- for SADE portal usage (such as Fontane, BdN, Architrave)
- for complex Digital Editions
- screencasts

- A README(.md) `MUST` be available in the source code repository (cf. [General Notions about Documentation](#general-notions-about-documentation))
- A more detailed explanation `SHOULD` be available for the user at some place (such as user guide in wikis, etc. pp.)
- Docs `SHOULD` be provided in a `docs` directory in the source code repository
- Docs `SHOULD` revised regularly (e.g. during doc sprints)

#### Further Reading

<http://writethedocs.org> is dedicated to writing good documentation.
At times it is a bit general but can serve you well to get a basic idea of what good documentation can look like.

[This blog post](https://www.altexsoft.com/blog/business/technical-documentation-in-software-development-types-best-practices-and-tools/)
provides a general overview of documentation types in software development, with a focus on agile workflows.
