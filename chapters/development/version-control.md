
### Version Control

#### Policy

Developers `MUST` use `git` for version control.
Please see <https://git-scm.com/doc> for documentation.

Every repository `SHOULD` be copied to and synced with a version control server (VCS).

Repositories `SHOULD` follow a branching model.

#### Guidelines

#### Version Control Servers

Currently we use the following VCSs:

- Projects (GWDG): <https://projects.gwdg.de>
- GitLab (GWDG): <https://gitlab.gwdg.de>
- GitHub: <https://github.com/subugoe>

Which one is suitable for you depends on:

- your project
- existing code
- whether or not you want to use CI/CD or GitLab Runners
- ...

Consider mirroring of repos for project visibility (e.g. mirror GitLab/Projects code to GitHub).

#### Branching Models

Features `SHOULD` be bundled in a separate branch before committing them to the default branch.

We recommend to use the [GitLab](https://docs.gitlab.com/ee/topics/gitlab_flow.html)/
[GitHub](https://githubflow.github.io/) Flow.

In some cases it can make sense to use [Git Flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow),
though.
Also consult the [Git Flow Cheat Sheet](https://danielkummer.github.io/git-flow-cheatsheet).

When using any kind of Flow it is good practice to protect your `main` and `develop` branch server-side.
This avoids accidental pushes to these branches.

##### Branch Naming Schema

All specific branches working on an issue described in a bug tracker `SHOULD` utilize the following naming scheme:
\
`ISSUE_NUMBER-DESCRIPTION`\
\

e.g. `12-flux-capacitor`.
Using the GitLab/GitHub UI for creating a merge/pull request will automatically apply this scheme.
Also keep in mind GitLab Flow's [11 Rules](https://about.gitlab.com/blog/2016/07/27/the-11-rules-of-gitlab-flow/).

##### Issue Handling

It is also `RECOMMENDED` to automatically close issues via commit message;
How this works exactly depends on the Git repository server.
Issues can also be [referenced across repositories](https://help.github.com/articles/autolinked-references-and-urls/#commit-shas).

#### Codebase

**TODO** Add RDD suggestions for “Twelve-Factor App: I. Codebase“

#### Further Reading

For more information on Git and related workflows, refer to the
[Atlassian tutorial](https://www.atlassian.com/git/tutorials/comparing-workflows).
