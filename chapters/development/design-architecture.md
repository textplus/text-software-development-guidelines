
### Software Design / Architecture

#### Configuration

 **TODO** Add RDD suggestions for “Twelve-Factor App: III. Config“

#### Robustness and Scalability (Processes)

An application is run by one ore more processes. These `SHOULD` be stateless
(share nothing). Startup and shutdown times of processes `SHOULD` be minimal.

In software design, processes `MAY` be used to distribute tasks according to
their type, so there may be short running web processes handling
HTTP requests and long running worker tasks.

All data `SHOULD` be stored in backing services, such as databases, for persistence.
Session state and data `SHOULD` also be handled by backing services.
Data stores with time-expiration like memcached or redis are useful for this.

Processes `SHOULD` handle a [SIGTERM](http://en.wikipedia.org/wiki/SIGTERM) correctly,
which means they `SHOULD` finish current tasks before exiting.
Processes `SHOULD` be robust to sudden death.

Services like [RabbitMQ](http://www.rabbitmq.com/) or
[Beanstalkd](https://beanstalkd.github.io/) `MAY` be used for handling long
running jobs, allowing workers to return their job to the work queue on
SIGTERM. Backends like beanstalkd can also be configured to return jobs to the
queue automatically when clients disconnect or time out, which may increase
robustness to sudden death.

Designing software this way can be beneficial to scaling.

References:

- [Twelve Factor App - Processes](https://12factor.net/processes)
- [Twelve Factor App - Concurrency](https://12factor.net/concurrency)
- [Twelve Factor App - Disposability](https://12factor.net/disposability)

#### Port Binding (Container Specific)

If a web service is shipped in a container it `MUST` provide a port where it is
reachable via the HTTP protocol. The container may ship an Apache HTTP Server
or the application may declare a library for serving HTTP as a dependency.

The container `MUST` `NOT` provide SSL encryption,
as this is the job of a front end server.
