
### Intellectual Property

Software developed in FE `MUST` fulfill the requirements of the [REUSE Specification](https://reuse.software/spec/).
Assistance may be obtained using the [reuse CLI tool](https://github.com/fsfe/reuse-tool).

#### Copyright

Every repository `MUST` include the [generic copyright statement](https://reuse.software/faq/#many-copyright-statements)
"\[YYYY\] Georg-August-Universität Göttingen" in source code files. The code repository `MAY` be accompanied by an
[AUTHORS file](https://opensource.google/documentation/reference/releasing/authors)
that lists everyone who has contributed to the code and wants to be attributed.

#### Licensing

Every open source project's code `SHOULD` be licensed under the European Union Public Licence (EUPL-1.2).
Code or Content that is supposed to be released to the Public Domain `MUST` be licensed under Creative Commons Zero v1.0
Universal (CC0-1.0).

#### Example code file header

Python

```py
# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0
```
