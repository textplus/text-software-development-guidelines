
### Release Management

#### Conventional Commits and Conventional Changelog

You `SHOULD` follow the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) standard and choose a suitable
commit format, preferably the [Conventional Changelog format](https://github.com/conventional-changelog/conventional-changelog).

You `SHOULD` provide a [Conventional Changelog](https://github.com/conventional-changelog/conventional-changelog) to
document the evolution of your source code.
If you follow a commit convention, it is possible to generate a changelog from your commits.
To generate a changelog outside the release cycle of your software, you can use the
[Conventional Changelog CLI](https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-cli).

##### Enforcement

To enforce Conventional Commits with your project, we recommend to use Commitizen as a helper tool with a suitable adapter,
preferably the [commitizen adapter for the angular preset](https://github.com/commitizen/cz-conventional-changelog).

If you use GitLab for hosting your source code, you can also add a **push rule** to your group or repository.
Go to "Push Rules" on group level or select "Settings / Repository → Push Rules" in your project to enter a
regex that prohibits all contributors to push commit or branches with names that don't comply to your rule.

#### Semantic Versioning and Semantic Release

You `SHOULD` choose [Semantic Versioning](https://semver.org/) over any kind of [Sentimental Versioning](http://sentimentalversioning.org/).
Exceptions `MAY` be made in case of software that is delivered to a public audience and with releases of different support
periods.

Doing a release with Semantic Versioning is often called a Semantic Release.
You `SHOULD` automate all steps in the process of a Semantic Release.
To help with the creation of a changelog, release notes and a GitLab release, use the npm tool [semantic-release](https://semantic-release.gitbook.io/semantic-release/).
