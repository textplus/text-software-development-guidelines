
### Code Quality

#### Code Reviews

Code reviews `SHOULD` take place for all commits and merge requests that target the default branch and release branches.
For these commits, developers should contact the developer most familiar with the technology. In particular, teams with
only developer, should work cross-team.

##### Workflow

1. Create an issue describing the target changes, briefly stating the feature or fix.
1. Use the „Create merge request“ button at the issue view. This creates a branch with a meaningful name related to the
   issue, and a merge request (MR). The MR has draft status by default. (This step matches the GitLab Flow.)
1. Checkout the branch locally and start implementing.
1. Commit and push your changes.
1. Remove the draft status of the MR if the changes meet the issue objective.
   You `MUST` assign and contact one suitable reviewer.
1. The reviewer must examine the changes regarding the fulfillments of the issue, coding language, understandability,
   documentation. The reviewers suggest improvements or approve the MR.
1. The developer merges the branch if the review was successful.

#### Code Quality Measurement with GitLab-CI

GitLab-CE provides some tools which can be used
[to measure code quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html).
We want to test these tools in the near future.
