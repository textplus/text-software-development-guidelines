
### Software Bill of Materials (SBOM)

A Software Bill of Materials (SBOM) is a file that keeps track of all the components a piece of software uses.
This also includes the components' licenses as well as their versions.
The aims of a SBOM are to increase transparency of the software supply chain and to easily detect security risks and
quality defects in dependencies.

#### Policy

Every repository contains a regularly updated SBOM file that is uploaded to SUB's instance of [Dependency Track](#using-dependency-track).

#### SBOM Creation and Tooling

There are a lot of tools available for conveniently creating SBOMs for your codebase.
Generally, the project [CycloneDX](https://cyclonedx.org/tool-center/) provides a good overview for tools at your service.

In RDD, we currently recommend using the following:

- **Python**: [cdxgen](https://github.com/AppThreat/cdxgen) >= 4.0.19
- **Java**: [CycloneDX Maven Plugin](https://github.com/CycloneDX/cyclonedx-maven-plugin) / [CycloneDX Gradle Plugin](https://github.com/CycloneDX/cyclonedx-gradle-plugin)
- **NodeJS**: [cdxgen](https://github.com/AppThreat/cdxgen) >= 4.0.19
- **Container images**: [syft](https://github.com/anchore/syft)

#### Using Dependency Track

To fully utilize your project's SBOM, it shouldn't just be part of your repository but put to further use.
We provide [Dependency Track](https://deps.sub.uni-goettingen.de) to monitor security risks in dependencies.
Dependency Track consumes SBOMs to identify components and checks them for known security vulnerabilites.

Only an up-to-date SBOM is of value, thus it `MUST` automatically be updated and uploaded to our Dependency Track
instance and audited regularly before deploying your application to a server to minimize attack vectors.

See also the following two templates for GitLab CI that might come in handy:

- [.deps.gitlab-ci.yml](https://gitlab.gwdg.de/dariah-de/gitlab-templates/-/blob/main/templates/.deps.gitlab-ci.yml)
- [SBOM-Upload.gitlab-ci](https://gitlab.gwdg.de/dariah-de/gitlab-templates/-/blob/main/templates/SBOM-Upload.gitlab-ci.yml)

`.deps.gitlab-ci.yml` takes care of uploading a SBOM to dependency track and could be used like in the following
[example](https://gitlab.gwdg.de/era-public/plan/-/blob/2f4725b17d5653e513b29633eff7608964c8d2ad/.gitlab-ci.yml#L65):

```yaml
include:
  - remote: https://gitlab.gwdg.de/dariah-de/gitlab-templates/-/raw/main/
    templates/.deps.gitlab-ci.yml

upload-container-sbom:
  stage: deploy
  only:
   - develop
  variables:
    X_API_KEY: $DEPS_UPLOAD_TOKEN
    AUTO_CREATE_PROJECT: 'true'
    PROJECT_NAME: $CI_PROJECT_NAME-container
    PROJECT_VERSION: develop
    BOM_LOCATION: 'sbom.container.json'
  extends:
    - .upload-bom-to-deps
```

`SBOM-Upload.gitlab-ci` utilizes `.deps.gitlab-ci.yml` but takes more values from the environment. It works fine in
cases where you have a development branch and you are using tags only for relases. It also assumes that your artifact
containing the SBOM is named `sbom.json` and located in the root.

This is then just
[included](https://gitlab.gwdg.de/dariah-de/textgridlab-marketplace/-/blob/514dda2978328f0a4569b6245acacb05e0d91ec2/.gitlab-ci.yml#L7)
and takes care of the rest:

```yaml
include:
  - remote: https://gitlab.gwdg.de/dariah-de/gitlab-templates/-/raw/main/
    templates/SBOM-Upload.gitlab-ci.yml
```
