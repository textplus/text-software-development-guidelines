
### Helpful Links and References

This section collects resources we find useful.
Feel free to add more if you happen to stumble upon anything interesting!

#### Other References

- EURISE-network technical-reference:\
<https://github.com/eurise-network/technical-reference>
- The Software Sustainability Institute, Guidelines and Publications:\
<https://www.software.ac.uk>
- Software Quality Guidelines:\
<https://github.com/CLARIAH/software-quality-guidelines>

- Software Testing Levels:\
<http://softwaretestingfundamentals.com/software-testing-levels>

- Netherlands eScience Center Guide\
<https://guide.esciencecenter.nl>

#### Communities

- DHTech -- An international grass-roots community of Digital Humanities software engineers:\
<https://dh-tech.github.io>

#### Software Quality

- The Twelve-Factor App:\
<https://12factor.net>
- The Joel Test: 12 Steps to Better Code:\
<https://www.joelonsoftware.com/2000/08/09/the-joel-test-12-steps-to-better-code>
- An introduction to clean code:\
<https://www.freecodecamp.org/news/clean-coding-for-beginners/>
[Freecodecamp](https://www.freecodecamp.org) has lots of other interesting articles, too, so check them out!

#### Improving the Sustainability of Webites

- <https://www.websitecarbon.com/>
- <https://solar.lowtechmagazine.com/2018/09/how-to-build-a-lowtech-website.html>
- <https://www.wholegraindigital.com/blog/website-energy-efficiency/>
- <https://cloudblogs.microsoft.com/industry-blog/en-gb/technetuk/2021/10/12/how-to-measure-and-reduce-the-carbon-footprint-of-your-application/>

#### Certification

- <https://www.blauer-engel.de/de/produktwelt/ressourcen-und-energieeffiziente-softwareprodukte>
