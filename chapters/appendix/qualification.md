
### Qualification and Training

Staying on top of your game of your technical skills is important, and fun.
Due to the sheer number of possibilities available, it is difficult to provide a
concise guide discussing qualification exhaustively. In this chapter, we try to
collect useful resources ranging from references for single programming languages
to course registries.

#### *Bildungsurlaub*

As an employee, you are entitled to *Bildungsurlaub*. You are entitled to up to
five days paid leave to participate in officially approved courses. Approving
agency is the [Agentur für Erwachsenen- und Weiterbildung (AEWB)](https://www.aewb-nds.de/bildungsurlaub/informationen/).
Sadly, there is no official register of recognized courses.
There is no official register of recognized courses.
One larger but commercial offer is [bildungsurlaub.de](https://www.bildungsurlaub.de/home.html).

**A note on *Bildungsurlaub***:  *BU* is intended for your own personal advancement.
You *can* use your *BU* for courses covering professional skills, but there are
possibilities to use *BU* for non-work related courses. Use your *BU* for work
at your own discretion.

#### Official Programs

- [Erasmus+](https://www.erasmusplus.de/) (cf. the relevant page at [uni-goettingen.de](https://www.uni-goettingen.de/de/erasmus%2B+key+action+103%3A+mobilit%C3%A4t+von+einzelpersonen+innerhalb+europas/475950.html))
- [International Software Testing Qualifications Board](https://www.istqb.org/)

#### Course Registries

- [GWDG Academy](https://www.gwdg.de/de/academy)
- [heise events](https://www.heise-events.de/)
- [Library Carpentry](https://librarycarpentry.org/lessons/)
- [Qualification offers for Uni employees](https://qualifizierung.uni-goettingen.de/)

#### Mixed Resources

- [CCC Media Archive](https://media.ccc.de/)
- [heise Veranstaltungskalender](https://www.heise.de/developer/termine/)

#### Learning Resources

##### Programming languages

###### General

- [w3schools](w3schools.com)

###### Docker

- [Docker reference](https://docs.docker.com/reference/)

###### HTML/CSS/SASS/SCSS

- [w3schools CSS reference](https://www.w3schools.com/cssref/default.asp)

###### Puppet

- [Puppet Cookbook](https://www.puppetcookbook.com/)
- [Puppet documentation](https://puppet.com/docs/puppet/5.5/puppet_index.html)

###### Python

- [Python documentation](https://docs.python.org/)
- [Full Stack Python](https://www.fullstackpython.com/)
- [PEP 8 -- Style Guide for Python Code](https://www.python.org/dev/peps/pep-0008/)

###### XQuery

- [eXist-db reference](https://www.exist-db.org/exist/apps/doc/)
- [XQuery reference](https://www.w3.org/TR/xquery-31/)
- [Data2Type Docs](https://www.data2type.de)

###### Other Markup languages

- [RST documentation](https://docutils.sourceforge.io/docs/ref/rst/restructuredtext.html)
- [Markdown reference](https://daringfireball.net/projects/markdown/syntax)
