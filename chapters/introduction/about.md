
### About This Document

**Author**: The Software Quality Working Group

**Audience**: Software developers (currently of the Research and Development Department as well as the Software and
Service Development Group of the Digital Library Department) of the Göttingen State and University Library.

**Purpose**: This document provides guidelines and best practices for starting new as well as improving on existing
software projects.

Our goal is to establish better software quality by following standards the developer team has mutually agreed upon.
Roughly basing on the [EURISE Network Technical Reference](https://eurise-network.github.io/), these standards are
discussed, worked out, and decided on by the Software Quality Working Group.

**Status**: This document is a living document and will be extended as soon as the Software Quality Working Group has
agreed on a new standard for software projects in the SUB's Research and Development Department.
TODOs and addenda to this document are maintained at this project's [issue tracker](https://gitlab.gwdg.de/fe/technical-reference/issues/).

**Terminology**: The key words `MUST`, `MUST` `NOT`, `REQUIRED`, `SHALL`, `SHALL NOT`, `SHOULD`, `SHOULD` `NOT`,
`RECOMMENDED`, `MAY`, and `OPTIONAL` in this document are to be interpreted as described in [RFC 2119](https://tools.ietf.org/html/rfc2119).
