
### Operations and Maintenance

#### Puppet

We use Puppet for the configuration and setup of all FE-maintained servers.
The main puppet code is provided in GitLab <https://gitlab.gwdg.de/dariah-de-puppet>.
See the [project's README file](https://gitlab.gwdg.de/dariah-de-puppet/dariah_de_puppet/-/blob/master/Readme.md) for details.

The DARIAH-DE and TextGrid Repository module (dhrep) is contained in GitHub <https://github.com/DARIAH-DE/puppetmodule-dhrep>.

There is a task force between FE/GWDG for maintaining the Puppet code and related issues.
It plans and performs sprints for planning, implementing, and documenting current Puppet issues such as major updates
or discussing module usage.

#### Manual Maintenance and Administration

Administrative and maintenance tasks encompass manually triggered one-off operations, such as database migrations or
cleanup, or other arbitrary operations. They can be invoked through scripts or direct input in a REPL shell.

These tasks `SHOULD` run in the same environment as regular processes, using the same codebase and config. Code (such as
scripts) for these tasks `MUST` ship as part of the codebase. These processes `SHOULD` use the same technology as
regular processes, in the interest of dependency isolation.

#### Monitoring

##### Logging

Logging is neither centralized nor enforced (yet) and you have to provide a proper configuration for the logging agent yourself.
[General recommendations](https://www.loggly.com/ultimate-guide/managing-linux-logs/) apply.
An agent `MUST` `NOT` log at "DEBUG" level or beyond (e.g. "TRACE", "SILLY") in production environments to prevent
personal identifiable information or [other sensible data](https://en.wikipedia.org/wiki/Personal_data#European_Union)
to be aggregated. This means, configure the agent for level "INFO" or above if there is no level between "DEBUG" and "INFO".

##### Icinga

We use the CLARIN-D / CLARIAH-DE / DARIAH-DE monitoring system (Icinga2) for our RDD servers, which is currently
installed at <https://monitoring.clarin.eu/dashboard>.

Different kinds of probes are implemented, mostly HTTP checks for external HTTP function monitoring,
and NRPE checks for internal server probes such as memory checks, ping, or certificate validity.
NRPE probes are configured on the servers using Puppet.

The workflow for configuration and deployment is realized in different repositories using Travis and Gitlab CI.
The main repository is located at <https://github.com/clarin-eric/monitoring>.
The repository for the DARIAH-DE configuration is located at <https://github.com/clarin-eric/monitoring-dariah>,
its state is validated via [Gitlab CI](https://gitlab.gwdg.de/dariah-de/monitoring-dariah).
The DARIAH-DE configuration repository is checked out from the main repository every hour, and deployed as soon
as a merge to the main branch has been successfully tested.

More detailed information concerning administration and monitoring workflow can be found in the
[FE-develop Wiki](https://wiki.de.dariah.eu/pages/viewpage.action?pageId=115195756) and in the
[monitoring-dariah Github repository](https://github.com/clarin-eric/monitoring-dariah).

##### Real time statistics / metrics

To view real time metrics from our servers or applications we use [Grafana](https://grafana.com/),
which is available at <https://metrics.gwdg.de> inside GoeNet.
Grafana retrieves its data from influxdb.
[Telegraf](https://github.com/influxdata/telegraf) can be used to store data from the servers in that database.
It is easy to enable telegraf on puppet configured servers.
Telegraf stores metrics from the server in influxdb.

Some system statistics monitored by telegraf in our current puppet setup:

- CPU
- Memory
- Space
- Apache
- Tomcat usage
- ...

Telegraf collects statistics with input plugins. A list of plugins is
[available](https://github.com/influxdata/telegraf#input-plugins).

##### Security Monitoring

In order to keep an overview of the software components used in your project, you `SHOULD` provide a
Software Bill of Materials (SBOM) and check for security vulnerabilities regularly.
For more information on this topic, see [Software Bill of Materials (SBOM)](#software-bill-of-materials-sbom).
