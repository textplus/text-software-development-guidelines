# Text+ Software Development Guidelines

The Text+ Software Development Guidelines are based on the [Guidelines of RDD at the SUB Göttingen](https://gitlab.gwdg.de/fe/technical-reference/), from wich this repo is forked. This is work in progress, and Text+ specifics will be added.

Like in the original repo, releases will be available in multiple formats.
