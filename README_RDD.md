# RDD Technical Reference

Guidelines and references for software development in [RDD](https://www.sub.uni-goettingen.de/projekte-forschung/forschung-entwicklung/).

To create and validate main markdown document, please use

```bash
cat technical-reference-main.txt | while read LINE; do cat $LINE >> technical-reference.md ; done
npx markdownlint -o markdownlint-report.txt technical-reference.md
```

To compile the markdown file into a PDF document, run

```bash
pandoc --template=rdd.latex technical-reference.md \
  --output=technical-reference.pdf \
  --shift-heading-level-by=-1
```

or, for the use with docker,

```bash
docker run --rm -v $(pwd):/data pandoc/latex:2.9.1.1 \
  pandoc technical-reference.md \
  --output=technical-reference.pdf \
  --template=rdd.latex \
  --shift-heading-level-by=-1
```

For convenience, a `build` script is provided with this repo.

Inspired by the [EURISE Network Technical Reference](https://github.com/eurise-network/technical-reference).

## Sources and PDF

The sources and the PDF of this reference are available continuously with every release here: <https://gitlab.gwdg.de/fe/technical-reference/-/releases>

## Contributing

Commits have to be made following a certain convention that is defined by the
[Conventional Commit](https://www.conventionalcommits.org/en/v1.0.0/) standard.
This repo is configured to be used with [Commitizen](https://github.com/commitizen/cz-cli) for your convenience
and uses the [angular preset](https://github.com/conventional-changelog/conventional-changelog).

Install it and other development dependencies with

```bash
npm install
```

Commitizen is now automagically configured to be called on a `git commit`.

We have chosen the following two types of commits to be used for the work on the technical reference
that should reflect the extent of your contribution:

- `fix`: Small addition to or revision of the Technical Reference's Markdown files.
- `feat`: Medium addition to or revision of the Technical Reference's Markdown files.

These commit types trigger a `patch` or `minor` release when merged into the `main` branch.
To trigger a `major` release, please consult your fellow contributors and mark one of your commits with a `BREAKING CHANGE`.

The `main` branch is protected for direct commits, so please use feature branches and create merge request for a review
of your changes.
